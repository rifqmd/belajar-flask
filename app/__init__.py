# Memanggil library Flask
from flask import Flask

# Untuk menjelaskan nama modul yang digunakan, sehingga ketika folder lain memanggil folder app akan otomatis teridentifikasi.
app = Flask(__name__)

# Memanggil file routes (akan segera dibuat)
from app import routes
